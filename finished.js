const express = require('express');
const router = express.Router();
const reqlib = require('app-root-path').require;
const logger = reqlib('logger');

const jsf = require('json-schema-faker');
const util = require('util')
const chance = require('chance')
const faker = require('faker')
jsf.extend('chance', () => new chance.Chance());
jsf.extend('faker', () => faker);

var recentDays = 5;

var schema = {
  "type": "array",
  "minItems": 10,
  "maxItems": 20,
  "items": {
	  "type": "object",
	  "properties": {
	    "name": {
	      "type": "string",
	      "faker": "name.findName"
	    },
	    "price": {
	      "type": "integer",
		  "minimum": 100,
		  "maximum": 999
		},
		"time": {
			"type": "integer",
			"minimum": 3,
			"maximum": 12
		  },
		"desease": {
			"type": "string",
			"faker": "lorem.word" 
		  }
	  },
	  "required": [
	    "name",
	    "price", 
		"time",
		"desease"
	   ]
	  }
};

/* GET home page. */
router.get('/', (req, res) => {

  jsf.resolve(schema).then(sample => {
  	   logger.debug(util.inspect(sample, 
  	   	{showHidden: false, depth: null}));
	   
	   res.render('finished', 
	  	{ projects:  sample });
  });

  
});

module.exports = router;
